package com.example.cse438.blackjack

//class for the array used in the leaderboard
import java.io.Serializable
class Leader (): Serializable{
    private var username: String = ""
    private var winratio: Long = 0
    private var firebaseId: String = ""

    constructor(
        username: String,
        winratio: Long,
        firebaseId: String

    ) : this() {
        this.username = username
        this.winratio = winratio
        this.firebaseId = firebaseId
    }

    fun getUserName(): String {
        return this.username
    }

    fun getWinRatio(): Long {
        return this.winratio
    }

    fun getFireBaseId(): String {
        return this.firebaseId
    }
}