package com.example.cse438.blackjack

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.TargetApi
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.example.cse438.blackjack.util.CardRandomizer
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_black_jack.*
import kotlinx.android.synthetic.main.activity_leader_board.*
import java.util.*
import kotlin.collections.HashMap

// activity class for the actual game of black jack
class BlackJackActivity : AppCompatActivity() {

    private lateinit var mDetector: GestureDetectorCompat
    private var height: Int = 0
    private var width: Int = 0
    private lateinit var dealerCardA: ImageView
    private lateinit var dealerCardB: ImageView
    private lateinit var playerCardA: ImageView
    private lateinit var playerCardB: ImageView
    private var dealerTotal: Int = 0
    private var playerTotal: Int = 0
    private var hitCount: Int = 1
    private var dHitCount: Int = 1
    private var playerTurn: Boolean = true
    private var winInt: Long = 0
    private var lossInt: Long = 0
    private var curMoney: Long = 0
    private var bet: Long = 0


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_black_jack)


        // gets data of the current user logged in from firestore
        val userId = FirebaseAuth.getInstance().currentUser?.uid
        val db = FirebaseFirestore.getInstance()
        if (userId != null) {
            db.collection("users").document(userId).get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        Log.d("BJA", "DocumentSnapshot data: ${document.data}")
                    } else {
                        Log.d("BJA", "No such document")
                    }
                    val data: MutableMap<String, Any>? = document.data
                    val username = data?.get("username") as String
                    val win = data?.get("win") as Long
                    val loss = data?.get("lose") as Long

                    winInt = win
                    lossInt = loss

                    main_userText.text = "User: " + username.toString()
                    main_winText.text = "Win: " + win.toString()
                    main_lossText.text = "Loss: " + loss.toString()
                }
                .addOnFailureListener { exception ->
                    Log.d("BJA", "get failed with ", exception)
                }
        }

        dealerCardA = dealerA
        dealerCardB = dealerB
        playerCardA = playerA
        playerCardB = playerB

        mDetector = GestureDetectorCompat(this, MyGestureListener())

        val metrics = this.resources.displayMetrics
        this.height = metrics.heightPixels
        this.width = metrics.widthPixels

        //leaderboard button action
        lb_btn.setOnClickListener {
            val intent = Intent(this, LeaderBoardActivity::class.java)
            startActivity(intent)
        }

        //start button action
        startBtn.setOnClickListener {

            //call of the data is repeated here to dynamically update the win/loss count and the current balance of the player

            val userId = FirebaseAuth.getInstance().currentUser?.uid
            val db = FirebaseFirestore.getInstance()
            if (userId != null) {
                db.collection("users").document(userId).get()
                    .addOnSuccessListener { document ->
                        if (document != null) {
                            Log.d("BJA", "DocumentSnapshot data: ${document.data}")
                        } else {
                            Log.d("BJA", "No such document")
                        }
                        val data: MutableMap<String, Any>? = document.data
                        val username = data?.get("username") as String
                        val win = data?.get("win") as Long
                        val loss = data?.get("lose") as Long
                        val money = data?.get("money") as Long

                        winInt = win
                        lossInt = loss
                        curMoney = money

                        main_userText.text = "User: " + username.toString()
                        main_winText.text = "Win: " + win.toString()
                        main_lossText.text = "Loss: " + loss.toString()
                    }
                    .addOnFailureListener { exception ->
                        Log.d("BJA", "get failed with ", exception)
                    }
            }

            //creates dialog so the user can enter how much they want to bet
            val dialogTotal = AlertDialog.Builder(this)
            val dialogTotalLayout = LinearLayout(this)
            val dialogTotalEditText = EditText(this)

            dialogTotalEditText.hint = "Bet! Your total: $$curMoney"
            dialogTotalEditText.setSingleLine()
            dialogTotalLayout.orientation = LinearLayout.VERTICAL
            dialogTotalLayout.addView(dialogTotalEditText)
            dialogTotalLayout.setPadding(50, 50, 50, 50)
            dialogTotal.setView(dialogTotalLayout)
            dialogTotal.setCancelable(false)

            dialogTotal.setPositiveButton("Enter") { dialogInterface: DialogInterface, i: Int ->
                if (dialogTotalEditText.text.isEmpty()){
                    Toast.makeText(this, "Please fill out the field", Toast.LENGTH_SHORT).show()
                }
                val betInput = dialogTotalEditText.text.toString()
                val betInputLong: Long = betInput.toLong()
                bet = betInputLong
            }

            dialogTotal.create().show()

            dealerTotal = 0
            playerTotal = 0
            hitCount = 1
            dHitCount = 1
            winLose.text = ""
            pTotal.text = "You got $$curMoney"

            playerTurn = true

            playerCardA.setImageResource(R.drawable.back)
            playerCardB.setImageResource(R.drawable.back)
            dealerCardA.setImageResource(R.drawable.back)
            dealerCardB.setImageResource(R.drawable.back)
            hit1.setImageResource(R.drawable.back)
            hit2.setImageResource(R.drawable.back)
            hit3.setImageResource(R.drawable.back)
            hit4.setImageResource(R.drawable.back)
            hit5.setImageResource(R.drawable.back)
            dHit1.setImageResource(R.drawable.back)
            dHit2.setImageResource(R.drawable.back)
            dHit3.setImageResource(R.drawable.back)
            dHit4.setImageResource(R.drawable.back)
            dHit5.setImageResource(R.drawable.back)

            moveTo(playerCardA, 0F, 0F)
            moveTo(playerCardB, 0F, 0F)
            moveTo(dealerCardA, 0F, 0F)
            moveTo(dealerCardB, 0F, 0F)
            moveTo(hit1, 0F, 0F)
            moveTo(hit2, 0F, 0F)
            moveTo(hit3, 0F, 0F)
            moveTo(hit4, 0F, 0F)
            moveTo(hit5, 0F, 0F)
            moveTo(dHit1, 0F, 0F)
            moveTo(dHit2, 0F, 0F)
            moveTo(dHit3, 0F, 0F)
            moveTo(dHit4, 0F, 0F)
            moveTo(dHit5, 0F, 0F)


            startBtn.isEnabled = false
            startBtn.text = "Game in progress"

            for (i in 1..4) {
                val randomizer: CardRandomizer = CardRandomizer()
                var cardList: ArrayList<Int> = randomizer.getIDs(this) as ArrayList<Int>
                val rand: Random = Random()

                val r: Int = rand.nextInt(cardList.size)
                val id: Int = cardList.get(r)

                var cardVal: Int = 0
                if (r < 9) {
                    cardVal = r + 1
                } else if (r == 13 || r == 26 || r == 39 || r == 52) {
                    cardVal = 10
                } else if (r == 10 || r == 11 || r == 12 || r == 23 || r == 24 || r == 25 || r == 36 || r == 37 || r == 38 || r == 49 || r == 50 || r == 51) {
                    cardVal = 10
                } else if (r == 9 || r == 22 || r == 35 || r == 48) {
                    if (playerTotal < 11) {
                        cardVal = 11
                    } else {
                        cardVal = 1
                    }

                } else if (r >= 14 && r < 22) {
                    cardVal = r - 12
                } else if (r >= 27 && r < 35) {
                    cardVal = r - 25
                } else if (r >= 40 && r < 48) {
                    cardVal = r - 38
                }


                val name: String = resources.getResourceEntryName(id)
                if (i == 1) {
                    playerCardA.setImageResource(id)
                    moveTo(playerCardA, -350F, 500F)
                    playerTotal += cardVal
                }
                if (i == 2) {
                    moveTo(dealerCardA, 350F, -500F)
                    dealerTotal += cardVal
                }
                if (i == 3) {
                    playerCardB.setImageResource(id)
                    moveTo(playerCardB, -250F, 500F)
                    playerTotal += cardVal
                }

                if (i == 4) {
                    dealerCardB.setImageResource(id)
                    moveTo(dealerCardB, 250F, -500F)
                    dealerTotal += cardVal

                    //if initial two cards for the dealer add up to 21, player loses
                    if(dealerTotal == 21){
                        winLose.text = "You Lost!"
                        lossInt += 1
                        curMoney -= bet
                        playerTurn = false

                        val userId = FirebaseAuth.getInstance().currentUser?.uid
                        val db = FirebaseFirestore.getInstance()
                        val winRatio: Long = 3*winInt - lossInt

                        db.collection("users").document(userId!!).update("lose", lossInt)
                        db.collection("users").document(userId!!).update("winrate", winRatio)
                        db.collection("users").document(userId!!).update("money", curMoney)

                        startBtn.isEnabled = true
                        startBtn.text = "Start"

                    }
                }
            }

        }

        signOut_btn.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }

    //detects the touch
    override fun onTouchEvent(event: MotionEvent) : Boolean {
        mDetector.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    //animation to move around cards
    fun moveTo(target: ImageView, targetX: Float, targetY: Float) {

        val animSetXY = AnimatorSet()

        val x = ObjectAnimator.ofFloat(
            target,
            "translationX",
            target.translationX,
            targetX
        )

        val y = ObjectAnimator.ofFloat(
            target,
            "translationY",
            target.translationY,
            targetY
        )

        animSetXY.playTogether(x, y)
        animSetXY.duration = 300
        animSetXY.start()
    }

    //decides action to get performed based on the user's touch input/aka gesture
    private inner class MyGestureListener : GestureDetector.SimpleOnGestureListener() {

        private var swipedistance = 150

        //since double tap means the user ends his/her action, the game gets played to the end
        //checks who one and updates the data accordingly
        override fun onDoubleTap(e: MotionEvent?): Boolean {
            val userId = FirebaseAuth.getInstance().currentUser?.uid
            val db = FirebaseFirestore.getInstance()

            playerTurn = false
            while (dealerTotal < 17) {
                val randomizer: CardRandomizer = CardRandomizer()
                var cardList: ArrayList<Int> = randomizer.getIDs(this@BlackJackActivity) as ArrayList<Int>
                val rand: Random = Random()

                val r: Int = rand.nextInt(cardList.size)
                val id: Int = cardList.get(r)
                val name: String = resources.getResourceEntryName(id)


                var cardVal: Int = 0
                if (r < 9) {
                    cardVal = r + 1
                }
                else if (r == 13 || r == 26 || r == 39 || r == 52) {
                    cardVal = 10
                }
                else if (r == 10 || r == 11 || r == 12 || r == 23 || r == 24 || r == 25 || r == 36 || r == 37 || r == 38 || r == 49 || r == 50 || r == 51) {
                    cardVal = 10
                }
                else if (r == 9 || r == 22 || r == 35 || r == 48) {
                    if (playerTotal < 11) {
                        cardVal = 11
                    }
                    else {
                        cardVal = 1
                    }

                }
                else if (r >= 14 && r < 22) {
                    cardVal = r - 12
                }
                else if (r >= 27 && r < 35) {
                    cardVal = r - 25
                }
                else if (r >= 40 && r < 48) {
                    cardVal = r - 38
                }


                if (dHitCount == 1) {
                    dHit1.setImageResource(id)
                    moveTo(dHit1, 150F, -500F)
                }
                else if (dHitCount == 2) {
                    dHit2.setImageResource(id)
                    moveTo(dHit2, 50F, -500F)
                }
                else if (dHitCount == 3) {
                    dHit3.setImageResource(id)
                    moveTo(dHit3, -50F, -500F)
                }
                else if (dHitCount == 4) {
                    dHit4.setImageResource(id)
                    moveTo(dHit4, -150F, -500F)
                }
                else if (dHitCount == 5) {
                    dHit5.setImageResource(id)
                    moveTo(dHit5, -250F, -500F)
                }

                dHitCount++
                dealerTotal += cardVal

            }
            if(playerTotal < 22 && dealerTotal < 22) {
                if (playerTotal > dealerTotal) {
                    winLose.text = "You Won!"
                    winInt += 1
                    curMoney += bet
                }
                else if (playerTotal < dealerTotal){
                    winLose.text = "You Lost!"
                    lossInt += 1
                    curMoney -= bet
                }
                else if (playerTotal == dealerTotal) {
                    winLose.text = "Tie"
                }
            }
            else if (dealerTotal > 21) {
                winLose.text = "You Won!"
                winInt += 1
                curMoney += bet
            }
            else if (playerTotal > 21) {
                winLose.text = "You Lost!"
                lossInt += 1
                curMoney -= bet
            }

            val winRatio: Long = 3*winInt - lossInt

            db.collection("users").document(userId!!).update("win", winInt)
            db.collection("users").document(userId!!).update("lose", lossInt)
            db.collection("users").document(userId!!).update("winrate", winRatio)
            db.collection("users").document(userId!!).update("money", curMoney)


            startBtn.isEnabled = true
            startBtn.text = "Start"



            return true
        }

        //since a user can lose during his turn, there is an if statement detecting if the player's total went over 21
        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            if (playerTurn && playerTotal < 22){
                if (e2.x - e1.x > swipedistance) {
                    val randomizer: CardRandomizer = CardRandomizer()
                    var cardList: ArrayList<Int> = randomizer.getIDs(this@BlackJackActivity) as ArrayList<Int>
                    val rand: Random = Random()

                    val r: Int = rand.nextInt(cardList.size)
                    val id: Int = cardList.get(r)
                    val name: String = resources.getResourceEntryName(id)

                    var cardVal: Int = 0
                    if (r < 9) {
                        cardVal = r + 1
                    }
                    else if (r == 13 || r == 26 || r == 39 || r == 52) {
                        cardVal = 10
                    }
                    else if (r == 10 || r == 11 || r == 12 || r == 23 || r == 24 || r == 25 || r == 36 || r == 37 || r == 38 || r == 49 || r == 50 || r == 51) {
                        cardVal = 10
                    }
                    else if (r == 9 || r == 22 || r == 35 || r == 48) {
                        // TODO: how should we set aces. Is there way for it to represent 1 and 11?
                        if (playerTotal < 11) {
                            cardVal = 11
                        }
                        else {
                            cardVal = 1
                        }

                    }
                    else if (r >= 14 && r < 22) {
                        cardVal = r - 12
                    }
                    else if (r >= 27 && r < 35) {
                        cardVal = r - 25
                    }
                    else if (r >= 40 && r < 48) {
                        cardVal = r - 38
                    }


                    if (hitCount == 1) {
                        hit1.setImageResource(id)
                        moveTo(hit1, -150F, 500F)
                    }
                    else if (hitCount == 2) {
                        hit2.setImageResource(id)
                        moveTo(hit2, -50F, 500F)
                    }
                    else if (hitCount == 3) {
                        hit3.setImageResource(id)
                        moveTo(hit3, 50F, 500F)
                    }
                    else if (hitCount == 4) {
                        hit4.setImageResource(id)
                        moveTo(hit4, 150F, 500F)
                    }
                    else if (hitCount == 5) {
                        hit5.setImageResource(id)
                        moveTo(hit5, 250F, 500F)
                    }
                    hitCount++
                    playerTotal += cardVal

                    if (playerTotal > 21) {
                        winLose.text = "You Lost!"
                        lossInt += 1
                        curMoney -= bet

                        val userId = FirebaseAuth.getInstance().currentUser?.uid
                        val db = FirebaseFirestore.getInstance()
                        val winRatio: Long = 3*winInt - lossInt

                        db.collection("users").document(userId!!).update("lose", lossInt)
                        db.collection("users").document(userId!!).update("winrate", winRatio)
                        db.collection("users").document(userId!!).update("money", curMoney)

                        startBtn.isEnabled = true
                        startBtn.text = "Start"
                    }
                    return true
                }
            }

            return false
        }
    }
}


