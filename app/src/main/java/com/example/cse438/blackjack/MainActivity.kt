package com.example.cse438.blackjack

import android.content.ContentValues
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import kotlinx.android.synthetic.main.activity_main.*

//either register new player or allow users to move to login page
class MainActivity : AppCompatActivity() {

    var firebaseAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        firebaseAuth = FirebaseAuth.getInstance()

        register_btn.setOnClickListener {
           registerUser()
        }

        logIn_btn.setOnClickListener {
            Log.d("MainActivity", "To login activity")
            val intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
        }
    }

    //register a user and create a database for the user when successfully registerd
    private fun registerUser() {
        val username = usernameIn.text.toString()
        val email = emailIn.text.toString()
        val password = pwIn.text.toString()

        if (username.isEmpty() || email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Please fill out the field", Toast.LENGTH_SHORT).show()
            return
        }

        Log.d("MainActivity", "Username is: " + username)
        Log.d("MainActivity", "Email is: " + email)
        Log.d("MainActivity", "Password is: " + password)
        firebaseAuth?.createUserWithEmailAndPassword(email, password)
            ?.addOnCompleteListener {
                if (!it.isSuccessful) return@addOnCompleteListener

                Log.d("Main", "Successfully registered with uid: ${it.result?.user?.uid}")

                val user = HashMap<String, Any>()

                user["username"] = username
                user["email"] = email
                user["win"] = 0
                user["lose"] = 0
                user["winrate"] = 0
                user["money"] = 1000

                FirebaseFirestore.getInstance().document("users/${firebaseAuth?.currentUser?.uid}")
                    .set(user)
                    .addOnSuccessListener { Log.d(ContentValues.TAG, "DocumentSnapshot successfully written") }
                    .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }
            }
            ?.addOnFailureListener {
                Log.d("Main", "Failed to register user: ${it.message}")
                Toast.makeText(this, "Failed to register user: ${it.message}", Toast.LENGTH_SHORT).show()
            }
    }

}
