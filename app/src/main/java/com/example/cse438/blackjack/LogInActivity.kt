package com.example.cse438.blackjack

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_log_in.*

//calls signinwithemailandpw method of firebaseauth to login and set the currentuser and move on the blackjack activity
//when successfully logged in
class LogInActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)

        logIn_fin_btn.setOnClickListener {
            val email = emailIn.text.toString()
            val password = pwIn.text.toString()

            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if (!it.isSuccessful) return@addOnCompleteListener

                    Log.d("Main", "Successfully signed in with uid: ${it.result?.user?.uid}")
                }
                .addOnFailureListener {
                    Log.d("Main", "Failed to sign in user: ${it.message}")
                    Toast.makeText(this, "Failed to sign in user: ${it.message}", Toast.LENGTH_SHORT).show()
                }

            val intent = Intent(this, BlackJackActivity::class.java)
            startActivity(intent)


        }

    }

}
