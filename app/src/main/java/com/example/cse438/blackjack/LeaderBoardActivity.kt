package com.example.cse438.blackjack

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import com.google.firebase.database.MutableData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_leader_board.*
import kotlinx.android.synthetic.main.gridviewitem.view.*

// when the user decides to view the leaderboard, shows the leaderboard
// using orderBy method of the firestore, shows the player in descending order
class LeaderBoardActivity : AppCompatActivity() {

    var LeaderList: ListView? = null
    var LeaderArray: ArrayList<Leader> = ArrayList()
    var testArray: ArrayList<String> = ArrayList()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leader_board)

        val db = FirebaseFirestore.getInstance()
        db.collection("users").orderBy("winrate", Query.Direction.DESCENDING).limit(10)
            .get()
            .addOnSuccessListener {result ->
                for (document in result) {
                    var arrayIndex = 0

                    Log.d("Leaderboard", "${document.id} => ${document.data}")
                    val data: MutableMap<String, Any>? = document.data
                    val username = data?.get("username") as String
                    val winratio = data?.get("winrate") as Long
                    val firebaseIden = document.id as String
                    Log.d("username", "$username")
                    Log.d("winratio", "$winratio")
                    Log.d("firebaseIden", "$firebaseIden")

                    LeaderArray.add(Leader(
                        username,
                        winratio,
                        firebaseIden
                    ))

                    testArray.add(username)
                }
                Log.d("Array", "$LeaderArray")
            }
            .addOnFailureListener {
                Log.d("Leaderboard", "Error getting documents: ", it)
            }
    }

    override fun onStart() {
        super.onStart()
        show_btn.setOnClickListener {
            LeaderList = findViewById(R.id.leaderboard_View)
            LeaderList?.adapter = boardAdapter(this, LeaderArray)
        }

    }

    //populates each row with the data stored in the array
    private class boardAdapter (context: Context, list: ArrayList<Leader>): BaseAdapter() {

        private val mContext: Context

        var leaderArray = list

        init {
            this.mContext = context
        }

        override fun getCount(): Int {
            return leaderArray.size
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return " "
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val rowMain = layoutInflater.inflate(R.layout.gridviewitem, parent, false)
            val userNameTextView = rowMain.findViewById<TextView>(R.id.userNameBin)
            val winRateTextView = rowMain.findViewById<TextView>(R.id.winRateBin)

            userNameTextView.text = leaderArray.get(position).getUserName()
            winRateTextView.text = leaderArray.get(position).getWinRatio().toString()

            return rowMain
        }
    }
}
