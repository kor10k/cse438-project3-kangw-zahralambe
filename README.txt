In this file you should include:
!!! We decided to give 3 points to a player if they win and take off a point from a player if they lose and for the leader board,it is based on the winpoint, which is calculated 3*win - lose !!! 
	- we implemented the ratio calculation this way because we felt this is the fairest way

Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
	- When I start a game, the data from the firestore gets partially pulled. It fully loads only after the second game starts.
	
* Is there anything that you did that you feel might be unclear? Explain it here.

A description of the creative portion of the assignment
* Describe your feature
	- We decided to have players bet money when playing
	
* Why did you choose this feature?
	- to add more entertainment to the game
	
* How did you implement it?
	- We added a money index in the document for each player
	- When player initially signs up, they are given $1000
	- When player choses to play a game, a dialogbox prompts the player to enter how much he/she wants to bet
	- When the player loses a game, they loses the amount of money they bet
	- When the player wins a game, they earn the amount of money they bet
	- The new total amount of money the player has gets updated with a firestore call
	
(10 points) Player can login and login data is stored in Firebase
(10 points) Player’s win/loss counts are displayed on startup
(10 points) The Player receives two cards face up, and the dealer receives one card face up and one card face down
(5 points) Swiping right allows the Player to hit
(5 points) Double tapping allows the Player to stand
(10 points) Cards being dealt are animated, and all cards are visible.
(5 points) If the Player goes over 21, they automatically bust
(10 points) The dealer behaves appropriately based on the rules described above
(10 points) Once the game is complete, the winner should be declared and the Firebase database should be updated appropriately
(5 points) A new round is automatically started after each round
(5 points) Player can logout
(10 points) A leaderboard is shown in a separate tab or activity and is consistent among installations of the app
(6 points) Creative portion!
	The creative portion is a good idea, but I have some issues with the implementation. There's no limit on the betting, the amount of money that I have doesn't matter at all. I can bet $1 million on one hand and if I lose, oh well, I'll just bet $2 the next time. That's not how betting works.
	As for the issue with data, some of the data loads, but the amount of money doesn't load until the second hand because of the fact that the database is an asynchronous call. This means that the dialog is constructed before the database call as finished. You should include this in the success listener to make sure you have the data first.
	

Total: 101 / 110
